class Game {
    constructor(name){
        this.name = name;
    }

    sayHello() {
        console.log("Hello " + this.name);
    }

    goodBye() {
        console.log("Goodbye " + this.name);
    }

    start() {
        console.log("Starting game...")
    }

    end() {
        console.log("Ending game for " + this.name + "!");
    }
}

module.exports = Game;
