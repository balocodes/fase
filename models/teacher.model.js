const mongoose = require("mongoose");

const schema = mongoose.Schema;

const teacherSchema = new schema({
    name: { type: String },
    surname: { type: String, required: true },
    class: { type: String, default: "Not Assigned"},
    level: { type: Number },
    is_principal: { type: Boolean, default: false },
    created_at: { type: Date, default: Date.now, once: true },
    updated_at: { type: Date, default: Date.now }
});

const teacherModel = mongoose.model("Teacher", teacherSchema);

module.exports = teacherModel;