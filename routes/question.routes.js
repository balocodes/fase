const router = require("express").Router();

const questionModel = require("../models/question.model");

router.post("/add-question", (req, res) => {
    let questionToSave = new questionModel({
        question: req.body.question,
        "options.option_a": req.body.option_a,
        "options.option_b": req.body.option_b,
        "options.option_c": req.body.option_c,
        correct_answer: req.body.correct_answer
    });

    questionToSave.save((err, data) => {
        if(err) {
            res.send("An error occured!");
        } else {
            res.send("Data saved succesfully!");
        }
    })

})

router.get("/all-questions", (req, res) => {
    questionModel.find((err, result) => {
        if(err) {
            res.send("An error occured!")
        } else {
            res.send(result);
        }
    })
})

router.get("/single-question", (req, res) => {
    questionModel.findById(req.query.id, (err, result) => {
        if(err) {
            res.send("An Error Occured!");
        } else {
            res.send(result);
        }
    })
})

router.post("/update-question/:id", (req, res) => {

})

router.post("/delete-question/:id", (req, res) => {

})

module.exports = router;